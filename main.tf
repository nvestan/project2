
resource "gitlab_deploy_key" "example" {
  project = "project2"
  title   = "my key"
  key     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+uo0uSCBdeQo7wNdNza+ZlRQFvAsVPAt4MeZKzZ+OzI+3MZAlekpqvb5vXVDz9lVtqZScwLk8kx18nw5gffvwMiSc1qHwbQLfwX+6Xizgp$
  can_push = "true"
}

variable "gitlab_token" {
  description = "Gitlab Token"
  default     = ""
}

provider "gitlab" {
    token = "${var.gitlab_token}"

}



# Add a project owned by the user
resource "gitlab_project" "sample_project" {
    name = "project2"
    visibility_level = "public"

}

